$(document).ready(function() {
    //Select 
    for (accesorio in accesorios) {
        //Crear elemento
        var elemento = document.createElement('option');
        
        //Setear Texto y Valor
        $(elemento).text(accesorios[accesorio].etiqueta);
        $(elemento).attr('value', accesorio);
        $(elemento).attr('data-price', accesorios[accesorio].precio);

        //Agregar option
        $('#accesorios').append(elemento);
    }

    $('#boton').click(function(event) {
        //Obtener Valor
        var valor = $('#accesorios').val();
        
        /*BUSCAR EN EL HTML */
        //Obtener Texto
        var texto = $('#accesorios option[value="' + valor + '"]').text();
        //Obtener Precio
        var precio = $('#accesorios option[value="' + valor + '"]').data('price');

        /*O BUSCAR EN EL JSON*/
        //var texto_json = accesorios[valor].etiqueta;
        //var precio_json = accesorios[valor].precio;

        //Obtener Cantidad
        var cantidad = $('#cantidad').val();        

        //Crear tr
        var tr = document.createElement('tr');

        //Crear los td
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        var td3 = document.createElement('td');

        //Agregar el Texto a los td
        $(td1).text(texto);
        $(td2).text(cantidad);
        $(td3).text((cantidad*precio).toFixed(2));         

        //Agregar los td al tr
        $(tr).append(td1);
        $(tr).append(td2);
        $(tr).append(td3);

        //Agregar el tr a la tabla
        $('#compra tbody').prepend(tr);

        //Obtener Precio
        var totales = $('#compra tbody tr td:nth-child(3):not(#totalval)');

        //Calcular Total
        var total = 0;
        $(totales).each(function(index, element) {
            total = total + parseInt($(element).text());
        });

       //Motrar Total
        $('#totalval').text(total.toFixed(2));
    });
});